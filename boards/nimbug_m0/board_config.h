#ifndef BOARD_CONFIG_H
#define BOARD_CONFIG_H

#define CRYSTALLESS    1

#define VENDOR_NAME "Nimrod Devices"
#define PRODUCT_NAME "Nimbug M0"
#define VOLUME_LABEL "NIMBUG_M0"
#define INDEX_URL "https://gitlab.com/nimrod-devices/Nimbug-M0"
#define BOARD_ID "SAMD21E18A-Nimbug-v0"

#define USB_VID 0x6942
#define USB_PID 0x001E

#define LED_PIN PIN_PA10
//#define LED_TX_PIN PIN_PA27
//#define LED_RX_PIN PIN_PB03

//#define BOARD_RGBLED_CLOCK_PIN            PIN_PA01
//#define BOARD_RGBLED_DATA_PIN             PIN_PA00

#endif
